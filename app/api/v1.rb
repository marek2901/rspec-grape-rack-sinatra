class ApiV1 < Grape::API
  version 'v1', using: :path
  format :json
  helpers V1Helpers
  rescue_from :all

  desc "Get a list of Widgets"
  get :widgets do
    widgets_response Widget.all
  end

  desc "Get a specific widget"
  get 'widget/:id' do
    widget_response Widget.find(params[:id])
  end
end
